<?php

namespace App\Entity;

use App\Repository\ClassroomRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ClassroomRepository::class)
 * @UniqueEntity("name")
 */
class Classroom
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Classroom name can not be blank")
     * @Assert\Length(min=2, max=254, minMessage="Name value is too short", maxMessage="Name value is too long")
     * @Assert\Type(type="string", message="name value should be type of {{ type }} ")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\Type(type="bool", message="is_active value should be type of {{ type }} ")
     */
    private $is_active;

    /**
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * @ORM\Column(type="datetime")
     */
    private $created_date;

    public function __construct() {
        $this->created_date = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDate(): \DateTimeInterface
    {
        return $this->created_date;
    }

    /**
     * @param \DateTime $created_date
     * @return $this
     */
    public function setCreatedDate(\DateTime $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function customJsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'isActive' => $this->is_active,
            'createdDate' => $this->created_date
        ];
    }
}

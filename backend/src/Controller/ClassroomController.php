<?php


namespace App\Controller;


use App\Entity\Classroom as ClassroomEntity;
use App\Model\Classroom;
use App\Repository\ClassroomRepository;
use App\Repository\UserRepository;
use App\Service\ConstraintValidator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClassroomController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ClassroomRepository
     */
    private $classroomRepository;
    /**
     * @var ConstraintValidator
     */
    private $constraintValidator;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var Classroom
     */
    private $classroom;

    public function __construct(
        Classroom $classroom,
        ClassroomRepository $classroomRepository,
        EntityManagerInterface $entityManager,
        ConstraintValidator $constraintValidator,
        ValidatorInterface $validator,
        SerializerInterface $serializer
    )
    {
        $this->classroom = $classroom;
        $this->entityManager = $entityManager;
        $this->classroomRepository = $classroomRepository;
        $this->constraintValidator = $constraintValidator;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }
    public function getAction(ClassroomEntity $classroom) {
        $classroomData = $this->serializer->serialize($classroom->customJsonSerialize(), 'json');
        return new Response($classroomData, Response::HTTP_OK);
    }

    public function cgetAction() {
        $classrooms = $this->classroomRepository->findAll();
        $classroomsData = $this->serializer->serialize($classrooms, 'json');
        return new Response($classroomsData, Response::HTTP_OK);
    }

    public function postAction(Request $request)
    {
        $requestData = json_decode($request->getContent(), true);
        $errors = [];

        if (!array_key_exists('name', $requestData)) {
            $errors[] = 'Empty name';
        }
        if (!array_key_exists('is_active', $requestData)) {
            $errors[] = 'Empty is_active';
        }
        if($errors) {
            return new Response(implode(",", $errors), Response::HTTP_BAD_REQUEST, array('Content-Type' => 'text/json'));
        }

        $classroom = $this->classroom->createClassroom([
            'name' => $requestData['name'],
            'is_active' => $requestData['is_active']
        ]);
        //create validator
        $validate_results = $this->constraintValidator->getValidateResponse($this->validator->validate($classroom));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return new Response(implode(",", $validate_results), Response::HTTP_BAD_REQUEST, array('Content-Type' => 'text/json'));

        $this->entityManager->persist($classroom);
        $this->entityManager->flush();

        return new Response('created', Response::HTTP_CREATED, array('Content-Type' => 'text/json'));
    }

    public function putAction(ClassroomEntity $classroom, Request $request)
    {
        $requestData = json_decode($request->getContent(), true);
        $classroom = $this->classroom->updateClassroom($classroom, [
            'name' => $requestData['name'] ?? $classroom->getName(),
            'is_active' => $requestData['is_active'] ?? $classroom->getIsActive()
        ]);

        //create validator
        $validate_results = $this->constraintValidator->getValidateResponse($this->validator->validate($classroom));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return new Response(implode(",", $validate_results), Response::HTTP_BAD_REQUEST, array('Content-Type' => 'text/json'));

        $this->entityManager->persist($classroom);
        $this->entityManager->flush();

        return new Response('updated', Response::HTTP_OK, array('Content-Type' => 'text/json'));
    }

    public function deleteAction(ClassroomEntity $classroom) {
        $this->entityManager->remove($classroom);
        $this->entityManager->flush();

        return new Response('deleted', Response::HTTP_OK, array('Content-Type' => 'text/json'));
    }
}
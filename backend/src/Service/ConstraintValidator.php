<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 0:44
 */

namespace App\Service;


use Symfony\Component\Validator\ConstraintViolationListInterface;

class ConstraintValidator
{
    /**
     * @param ConstraintViolationListInterface $violations
     * @return bool | array
     * May return errors, checks entity constraints
     */
    public function getValidateResponse(ConstraintViolationListInterface $violations)
    {
        if (0 !== count($violations)) {
            foreach ($violations as $violation) {
                $errors[] = $violation->getMessage();
            }

            return $errors ?? [];
        }

        return true;
    }
}
<?php


namespace App\Model;


use App\Entity\Classroom as ClassroomEntity;

class Classroom
{
    public function createClassroom(array $data) {
        $classroom = new ClassroomEntity();

        $classroom->setName($data['name']);
        $classroom->setIsActive($data['is_active']);

        return $classroom;
    }

    public function updateClassroom(ClassroomEntity $classroom, $data) {
        if(array_key_exists('name', $data)) {
            if (!is_null($data['name'])) {
                $classroom->setName($data['name']);
            }
        }

        if(array_key_exists('is_active', $data)) {
            if (!is_null($data['is_active'])) {
                $classroom->setIsActive($data['is_active']);
            }
        }

        return $classroom;
    }
}